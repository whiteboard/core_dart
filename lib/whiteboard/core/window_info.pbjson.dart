///
//  Generated code. Do not modify.
//  source: whiteboard/core/window_info.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use windowInfoDescriptor instead')
const WindowInfo$json = const {
  '1': 'WindowInfo',
  '2': const [
    const {'1': 'id', '3': 3, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'index', '3': 4, '4': 1, '5': 5, '10': 'index'},
    const {'1': 'title', '3': 5, '4': 1, '5': 9, '10': 'title'},
  ],
};

/// Descriptor for `WindowInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List windowInfoDescriptor = $convert.base64Decode('CgpXaW5kb3dJbmZvEg4KAmlkGAMgASgJUgJpZBIUCgVpbmRleBgEIAEoBVIFaW5kZXgSFAoFdGl0bGUYBSABKAlSBXRpdGxl');
