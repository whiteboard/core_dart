///
//  Generated code. Do not modify.
//  source: whiteboard/core/window.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'window_info.pb.dart' as $12;
import 'point.pb.dart' as $13;
import 'size.pb.dart' as $14;
import 'gauge.pb.dart' as $15;
import 'scrollbar.pb.dart' as $16;

import 'window_status.pbenum.dart' as $17;

class Window extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Window', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOM<$12.WindowInfo>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'info', subBuilder: $12.WindowInfo.create)
    ..aOM<$13.Point>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'point', subBuilder: $13.Point.create)
    ..aOM<$14.Size>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', subBuilder: $14.Size.create)
    ..aOM<$14.Size>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', subBuilder: $14.Size.create)
    ..aOM<$14.Size>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', subBuilder: $14.Size.create)
    ..aOM<$15.Gauge>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'margin', subBuilder: $15.Gauge.create)
    ..aOM<$15.Gauge>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'padding', subBuilder: $15.Gauge.create)
    ..aOM<$16.Scrollbar>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'scrollbar', subBuilder: $16.Scrollbar.create)
    ..e<$17.WindowStatus>(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: $17.WindowStatus.WINDOW_STATUS_UNSPECIFIED, valueOf: $17.WindowStatus.valueOf, enumValues: $17.WindowStatus.values)
    ..a<$core.int>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zIndex', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Window._() : super();
  factory Window({
    $12.WindowInfo? info,
    $13.Point? point,
    $14.Size? min,
    $14.Size? size,
    $14.Size? max,
    $15.Gauge? margin,
    $15.Gauge? padding,
    $16.Scrollbar? scrollbar,
    $17.WindowStatus? status,
    $core.int? zIndex,
  }) {
    final _result = create();
    if (info != null) {
      _result.info = info;
    }
    if (point != null) {
      _result.point = point;
    }
    if (min != null) {
      _result.min = min;
    }
    if (size != null) {
      _result.size = size;
    }
    if (max != null) {
      _result.max = max;
    }
    if (margin != null) {
      _result.margin = margin;
    }
    if (padding != null) {
      _result.padding = padding;
    }
    if (scrollbar != null) {
      _result.scrollbar = scrollbar;
    }
    if (status != null) {
      _result.status = status;
    }
    if (zIndex != null) {
      _result.zIndex = zIndex;
    }
    return _result;
  }
  factory Window.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Window.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Window clone() => Window()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Window copyWith(void Function(Window) updates) => super.copyWith((message) => updates(message as Window)) as Window; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Window create() => Window._();
  Window createEmptyInstance() => create();
  static $pb.PbList<Window> createRepeated() => $pb.PbList<Window>();
  @$core.pragma('dart2js:noInline')
  static Window getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Window>(create);
  static Window? _defaultInstance;

  @$pb.TagNumber(2)
  $12.WindowInfo get info => $_getN(0);
  @$pb.TagNumber(2)
  set info($12.WindowInfo v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasInfo() => $_has(0);
  @$pb.TagNumber(2)
  void clearInfo() => clearField(2);
  @$pb.TagNumber(2)
  $12.WindowInfo ensureInfo() => $_ensure(0);

  @$pb.TagNumber(3)
  $13.Point get point => $_getN(1);
  @$pb.TagNumber(3)
  set point($13.Point v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasPoint() => $_has(1);
  @$pb.TagNumber(3)
  void clearPoint() => clearField(3);
  @$pb.TagNumber(3)
  $13.Point ensurePoint() => $_ensure(1);

  @$pb.TagNumber(5)
  $14.Size get min => $_getN(2);
  @$pb.TagNumber(5)
  set min($14.Size v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMin() => $_has(2);
  @$pb.TagNumber(5)
  void clearMin() => clearField(5);
  @$pb.TagNumber(5)
  $14.Size ensureMin() => $_ensure(2);

  @$pb.TagNumber(6)
  $14.Size get size => $_getN(3);
  @$pb.TagNumber(6)
  set size($14.Size v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasSize() => $_has(3);
  @$pb.TagNumber(6)
  void clearSize() => clearField(6);
  @$pb.TagNumber(6)
  $14.Size ensureSize() => $_ensure(3);

  @$pb.TagNumber(7)
  $14.Size get max => $_getN(4);
  @$pb.TagNumber(7)
  set max($14.Size v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasMax() => $_has(4);
  @$pb.TagNumber(7)
  void clearMax() => clearField(7);
  @$pb.TagNumber(7)
  $14.Size ensureMax() => $_ensure(4);

  @$pb.TagNumber(10)
  $15.Gauge get margin => $_getN(5);
  @$pb.TagNumber(10)
  set margin($15.Gauge v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasMargin() => $_has(5);
  @$pb.TagNumber(10)
  void clearMargin() => clearField(10);
  @$pb.TagNumber(10)
  $15.Gauge ensureMargin() => $_ensure(5);

  @$pb.TagNumber(11)
  $15.Gauge get padding => $_getN(6);
  @$pb.TagNumber(11)
  set padding($15.Gauge v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasPadding() => $_has(6);
  @$pb.TagNumber(11)
  void clearPadding() => clearField(11);
  @$pb.TagNumber(11)
  $15.Gauge ensurePadding() => $_ensure(6);

  @$pb.TagNumber(13)
  $16.Scrollbar get scrollbar => $_getN(7);
  @$pb.TagNumber(13)
  set scrollbar($16.Scrollbar v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasScrollbar() => $_has(7);
  @$pb.TagNumber(13)
  void clearScrollbar() => clearField(13);
  @$pb.TagNumber(13)
  $16.Scrollbar ensureScrollbar() => $_ensure(7);

  @$pb.TagNumber(14)
  $17.WindowStatus get status => $_getN(8);
  @$pb.TagNumber(14)
  set status($17.WindowStatus v) { setField(14, v); }
  @$pb.TagNumber(14)
  $core.bool hasStatus() => $_has(8);
  @$pb.TagNumber(14)
  void clearStatus() => clearField(14);

  @$pb.TagNumber(15)
  $core.int get zIndex => $_getIZ(9);
  @$pb.TagNumber(15)
  set zIndex($core.int v) { $_setSignedInt32(9, v); }
  @$pb.TagNumber(15)
  $core.bool hasZIndex() => $_has(9);
  @$pb.TagNumber(15)
  void clearZIndex() => clearField(15);
}

