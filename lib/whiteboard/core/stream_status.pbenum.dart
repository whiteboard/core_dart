///
//  Generated code. Do not modify.
//  source: whiteboard/core/stream_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class StreamStatus extends $pb.ProtobufEnum {
  static const StreamStatus STREAM_UNSPECIFIED = StreamStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STREAM_UNSPECIFIED');
  static const StreamStatus TYPED = StreamStatus._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TYPED');
  static const StreamStatus PUSHED = StreamStatus._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PUSHED');
  static const StreamStatus PAUSED = StreamStatus._(20, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PAUSED');
  static const StreamStatus STOPPED = StreamStatus._(30, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STOPPED');

  static const $core.List<StreamStatus> values = <StreamStatus> [
    STREAM_UNSPECIFIED,
    TYPED,
    PUSHED,
    PAUSED,
    STOPPED,
  ];

  static final $core.Map<$core.int, StreamStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static StreamStatus? valueOf($core.int value) => _byValue[value];

  const StreamStatus._($core.int v, $core.String n) : super(v, n);
}

