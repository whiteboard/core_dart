///
//  Generated code. Do not modify.
//  source: whiteboard/core/scrollbar.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Scrollbar extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Scrollbar', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ratio', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Scrollbar._() : super();
  factory Scrollbar({
    $core.double? ratio,
  }) {
    final _result = create();
    if (ratio != null) {
      _result.ratio = ratio;
    }
    return _result;
  }
  factory Scrollbar.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Scrollbar.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Scrollbar clone() => Scrollbar()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Scrollbar copyWith(void Function(Scrollbar) updates) => super.copyWith((message) => updates(message as Scrollbar)) as Scrollbar; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Scrollbar create() => Scrollbar._();
  Scrollbar createEmptyInstance() => create();
  static $pb.PbList<Scrollbar> createRepeated() => $pb.PbList<Scrollbar>();
  @$core.pragma('dart2js:noInline')
  static Scrollbar getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Scrollbar>(create);
  static Scrollbar? _defaultInstance;

  @$pb.TagNumber(3)
  $core.double get ratio => $_getN(0);
  @$pb.TagNumber(3)
  set ratio($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasRatio() => $_has(0);
  @$pb.TagNumber(3)
  void clearRatio() => clearField(3);
}

