///
//  Generated code. Do not modify.
//  source: whiteboard/core/config.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use configDescriptor instead')
const Config$json = const {
  '1': 'Config',
  '2': const [
    const {'1': 'collapsed', '3': 3, '4': 1, '5': 8, '10': 'collapsed'},
    const {'1': 'prohibited', '3': 4, '4': 1, '5': 8, '10': 'prohibited'},
    const {'1': 'autostart', '3': 5, '4': 1, '5': 8, '10': 'autostart'},
    const {'1': 'host', '3': 9, '4': 1, '5': 11, '6': '.whiteboard.core.User', '10': 'host'},
    const {'1': 'device', '3': 10, '4': 1, '5': 11, '6': '.whiteboard.core.Device', '10': 'device'},
    const {'1': 'stream', '3': 11, '4': 1, '5': 11, '6': '.whiteboard.core.Stream', '10': 'stream'},
    const {'1': 'stage', '3': 12, '4': 1, '5': 11, '6': '.whiteboard.core.Stage', '10': 'stage'},
  ],
};

/// Descriptor for `Config`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List configDescriptor = $convert.base64Decode('CgZDb25maWcSHAoJY29sbGFwc2VkGAMgASgIUgljb2xsYXBzZWQSHgoKcHJvaGliaXRlZBgEIAEoCFIKcHJvaGliaXRlZBIcCglhdXRvc3RhcnQYBSABKAhSCWF1dG9zdGFydBIpCgRob3N0GAkgASgLMhUud2hpdGVib2FyZC5jb3JlLlVzZXJSBGhvc3QSLwoGZGV2aWNlGAogASgLMhcud2hpdGVib2FyZC5jb3JlLkRldmljZVIGZGV2aWNlEi8KBnN0cmVhbRgLIAEoCzIXLndoaXRlYm9hcmQuY29yZS5TdHJlYW1SBnN0cmVhbRIsCgVzdGFnZRgMIAEoCzIWLndoaXRlYm9hcmQuY29yZS5TdGFnZVIFc3RhZ2U=');
