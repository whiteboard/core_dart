///
//  Generated code. Do not modify.
//  source: whiteboard/core/device.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'webcam.pb.dart' as $1;
import 'microphone.pb.dart' as $2;

class Device extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Device', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOM<$1.Webcam>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'webcam', subBuilder: $1.Webcam.create)
    ..aOM<$2.Microphone>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'microphone', subBuilder: $2.Microphone.create)
    ..hasRequiredFields = false
  ;

  Device._() : super();
  factory Device({
    $1.Webcam? webcam,
    $2.Microphone? microphone,
  }) {
    final _result = create();
    if (webcam != null) {
      _result.webcam = webcam;
    }
    if (microphone != null) {
      _result.microphone = microphone;
    }
    return _result;
  }
  factory Device.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Device.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Device clone() => Device()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Device copyWith(void Function(Device) updates) => super.copyWith((message) => updates(message as Device)) as Device; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Device create() => Device._();
  Device createEmptyInstance() => create();
  static $pb.PbList<Device> createRepeated() => $pb.PbList<Device>();
  @$core.pragma('dart2js:noInline')
  static Device getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Device>(create);
  static Device? _defaultInstance;

  @$pb.TagNumber(3)
  $1.Webcam get webcam => $_getN(0);
  @$pb.TagNumber(3)
  set webcam($1.Webcam v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasWebcam() => $_has(0);
  @$pb.TagNumber(3)
  void clearWebcam() => clearField(3);
  @$pb.TagNumber(3)
  $1.Webcam ensureWebcam() => $_ensure(0);

  @$pb.TagNumber(4)
  $2.Microphone get microphone => $_getN(1);
  @$pb.TagNumber(4)
  set microphone($2.Microphone v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasMicrophone() => $_has(1);
  @$pb.TagNumber(4)
  void clearMicrophone() => clearField(4);
  @$pb.TagNumber(4)
  $2.Microphone ensureMicrophone() => $_ensure(1);
}

