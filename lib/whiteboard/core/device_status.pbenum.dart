///
//  Generated code. Do not modify.
//  source: whiteboard/core/device_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class DeviceStatus extends $pb.ProtobufEnum {
  static const DeviceStatus DEVICE_STATUS_UNSPECIFIED = DeviceStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'DEVICE_STATUS_UNSPECIFIED');
  static const DeviceStatus NO_SUCH = DeviceStatus._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NO_SUCH');
  static const DeviceStatus OPENED = DeviceStatus._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'OPENED');
  static const DeviceStatus CLOSED = DeviceStatus._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CLOSED');

  static const $core.List<DeviceStatus> values = <DeviceStatus> [
    DEVICE_STATUS_UNSPECIFIED,
    NO_SUCH,
    OPENED,
    CLOSED,
  ];

  static final $core.Map<$core.int, DeviceStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static DeviceStatus? valueOf($core.int value) => _byValue[value];

  const DeviceStatus._($core.int v, $core.String n) : super(v, n);
}

