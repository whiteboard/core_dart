///
//  Generated code. Do not modify.
//  source: whiteboard/core/user_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class UserType extends $pb.ProtobufEnum {
  static const UserType USER_TYPE_UNSPECIFIED = UserType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'USER_TYPE_UNSPECIFIED');
  static const UserType LECTURER = UserType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LECTURER');
  static const UserType ASSISTANT = UserType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ASSISTANT');
  static const UserType STUDENT = UserType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STUDENT');
  static const UserType TOURIST = UserType._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TOURIST');
  static const UserType INSPECTOR = UserType._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INSPECTOR');

  static const UserType SYSTEM = USER_TYPE_UNSPECIFIED;

  static const $core.List<UserType> values = <UserType> [
    USER_TYPE_UNSPECIFIED,
    LECTURER,
    ASSISTANT,
    STUDENT,
    TOURIST,
    INSPECTOR,
  ];

  static final $core.Map<$core.int, UserType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static UserType? valueOf($core.int value) => _byValue[value];

  const UserType._($core.int v, $core.String n) : super(v, n);
}

