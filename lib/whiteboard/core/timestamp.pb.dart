///
//  Generated code. Do not modify.
//  source: whiteboard/core/timestamp.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../google/protobuf/timestamp.pb.dart' as $11;

class Timestamp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Timestamp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'whiteboard.core'), createEmptyInstance: create)
    ..aOM<$11.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $11.Timestamp.create)
    ..aOM<$11.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'started', subBuilder: $11.Timestamp.create)
    ..aOM<$11.Timestamp>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'left', subBuilder: $11.Timestamp.create)
    ..aOM<$11.Timestamp>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'end', subBuilder: $11.Timestamp.create)
    ..aOM<$11.Timestamp>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ended', subBuilder: $11.Timestamp.create)
    ..hasRequiredFields = false
  ;

  Timestamp._() : super();
  factory Timestamp({
    $11.Timestamp? start,
    $11.Timestamp? started,
    $11.Timestamp? left,
    $11.Timestamp? end,
    $11.Timestamp? ended,
  }) {
    final _result = create();
    if (start != null) {
      _result.start = start;
    }
    if (started != null) {
      _result.started = started;
    }
    if (left != null) {
      _result.left = left;
    }
    if (end != null) {
      _result.end = end;
    }
    if (ended != null) {
      _result.ended = ended;
    }
    return _result;
  }
  factory Timestamp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Timestamp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Timestamp clone() => Timestamp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Timestamp copyWith(void Function(Timestamp) updates) => super.copyWith((message) => updates(message as Timestamp)) as Timestamp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Timestamp create() => Timestamp._();
  Timestamp createEmptyInstance() => create();
  static $pb.PbList<Timestamp> createRepeated() => $pb.PbList<Timestamp>();
  @$core.pragma('dart2js:noInline')
  static Timestamp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Timestamp>(create);
  static Timestamp? _defaultInstance;

  @$pb.TagNumber(3)
  $11.Timestamp get start => $_getN(0);
  @$pb.TagNumber(3)
  set start($11.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStart() => $_has(0);
  @$pb.TagNumber(3)
  void clearStart() => clearField(3);
  @$pb.TagNumber(3)
  $11.Timestamp ensureStart() => $_ensure(0);

  @$pb.TagNumber(4)
  $11.Timestamp get started => $_getN(1);
  @$pb.TagNumber(4)
  set started($11.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStarted() => $_has(1);
  @$pb.TagNumber(4)
  void clearStarted() => clearField(4);
  @$pb.TagNumber(4)
  $11.Timestamp ensureStarted() => $_ensure(1);

  @$pb.TagNumber(8)
  $11.Timestamp get left => $_getN(2);
  @$pb.TagNumber(8)
  set left($11.Timestamp v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasLeft() => $_has(2);
  @$pb.TagNumber(8)
  void clearLeft() => clearField(8);
  @$pb.TagNumber(8)
  $11.Timestamp ensureLeft() => $_ensure(2);

  @$pb.TagNumber(9)
  $11.Timestamp get end => $_getN(3);
  @$pb.TagNumber(9)
  set end($11.Timestamp v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasEnd() => $_has(3);
  @$pb.TagNumber(9)
  void clearEnd() => clearField(9);
  @$pb.TagNumber(9)
  $11.Timestamp ensureEnd() => $_ensure(3);

  @$pb.TagNumber(10)
  $11.Timestamp get ended => $_getN(4);
  @$pb.TagNumber(10)
  set ended($11.Timestamp v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasEnded() => $_has(4);
  @$pb.TagNumber(10)
  void clearEnded() => clearField(10);
  @$pb.TagNumber(10)
  $11.Timestamp ensureEnded() => $_ensure(4);
}

