///
//  Generated code. Do not modify.
//  source: whiteboard/core/serialize.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use serializeDescriptor instead')
const Serialize$json = const {
  '1': 'Serialize',
  '2': const [
    const {'1': 'error', '3': 5, '4': 1, '5': 9, '10': 'error'},
  ],
};

/// Descriptor for `Serialize`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serializeDescriptor = $convert.base64Decode('CglTZXJpYWxpemUSFAoFZXJyb3IYBSABKAlSBWVycm9y');
