///
//  Generated code. Do not modify.
//  source: whiteboard/core/window_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class WindowStatus extends $pb.ProtobufEnum {
  static const WindowStatus WINDOW_STATUS_UNSPECIFIED = WindowStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'WINDOW_STATUS_UNSPECIFIED');
  static const WindowStatus MINIMIZED = WindowStatus._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MINIMIZED');
  static const WindowStatus NORMAL = WindowStatus._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NORMAL');
  static const WindowStatus MAXIMIZED = WindowStatus._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MAXIMIZED');

  static const $core.List<WindowStatus> values = <WindowStatus> [
    WINDOW_STATUS_UNSPECIFIED,
    MINIMIZED,
    NORMAL,
    MAXIMIZED,
  ];

  static final $core.Map<$core.int, WindowStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static WindowStatus? valueOf($core.int value) => _byValue[value];

  const WindowStatus._($core.int v, $core.String n) : super(v, n);
}

