///
//  Generated code. Do not modify.
//  source: whiteboard/core/stream_status.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use streamStatusDescriptor instead')
const StreamStatus$json = const {
  '1': 'StreamStatus',
  '2': const [
    const {'1': 'STREAM_UNSPECIFIED', '2': 0},
    const {'1': 'TYPED', '2': 10},
    const {'1': 'PUSHED', '2': 11},
    const {'1': 'PAUSED', '2': 20},
    const {'1': 'STOPPED', '2': 30},
  ],
};

/// Descriptor for `StreamStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List streamStatusDescriptor = $convert.base64Decode('CgxTdHJlYW1TdGF0dXMSFgoSU1RSRUFNX1VOU1BFQ0lGSUVEEAASCQoFVFlQRUQQChIKCgZQVVNIRUQQCxIKCgZQQVVTRUQQFBILCgdTVE9QUEVEEB4=');
